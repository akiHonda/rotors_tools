#!/usr/bin/env python
import rospy
from gazebo_msgs.msg import ModelState
from rosgraph_msgs.msg import Clock
import math

class FakeMotion():

    def clock_callback(self, msg):
        #rospy.loginfo(msg.clock)
        if self.start_time == 0:
            self.start_time = msg.clock
            print(self.start_time)

        self.clock_flag = True

    def fake_motion(self):

        self.clock_flag = False
        self.start_time = 0

        pub = rospy.Publisher('/gazebo/set_model_state', ModelState, queue_size=10)
        rospy.Subscriber("clock", Clock, self.clock_callback)

        rospy.init_node('gazebo_fake_motion', anonymous=True)

        # Check ROSParam
        _model_name = rospy.get_param('/~model_name', 'firefly')
        _frame_name = rospy.get_param('/frame_name', 'world')

        r = rospy.Rate(100) # 100hz
        rospy.loginfo("[FakeMotion] Start. Model: %s, Ref Frame: %s", _model_name, _frame_name)

        motion_msg = ModelState()
        # Model name
        motion_msg.model_name = _model_name
        # Reference frame
        motion_msg.reference_frame = _frame_name

        while not rospy.is_shutdown():
            if self.clock_flag == True:
                # Model position & attitude
                motion_msg.pose.position.x = 1
                motion_msg.pose.position.y = 1
                motion_msg.pose.position.z = 1.0
                motion_msg.pose.orientation.x = 0.0
                motion_msg.pose.orientation.y = 0.0
                motion_msg.pose.orientation.z = 0.0
                motion_msg.pose.orientation.w = 1.0
                # Model velocity & angular rate
                motion_msg.twist.linear.x = 0.0
                motion_msg.twist.linear.y = 0.0
                motion_msg.twist.linear.z = 0.0
                motion_msg.twist.angular.x = 0.0
                motion_msg.twist.angular.y = 0.0
                motion_msg.twist.angular.z = 0.0

                pub.publish(motion_msg)

            r.sleep()

if __name__ == '__main__':

    try:
        fm = FakeMotion()
        fm.fake_motion()
    except rospy.ROSInterruptException: pass
